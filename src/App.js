import React from 'react';
import "./App.css"
import Presentation from "./Components/Presentation/Index";
import SkillsContainer from "./Components/SkillsScontainer";

const name = "Hernan";
const imgUrl = "https://i.pinimg.com/originals/0a/ec/a1/0aeca1c86f5d2a198c1e6d9b5024f851.jpg"; 
const description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
const roll = "soy estudiante";
const mySkills = [{name:"HTML",level:1},{name:"CSS",level:2},{name:"JavaScript",level:3}]

function App() {
  return (
    <div className="d-flex flex-column fondo">
      <header className="App-header">
        <Presentation 
          img = {imgUrl}
          name={name}
          roll={roll}
          description={description}
        />
      </header>
      
        <SkillsContainer skills={mySkills}/>

      
    </div>
  );
}

export default App;