import React ,{Component} from "react";

export default class Skill extends Component{


    getStringLevel(levelNumber){
        return (levelNumber === 1 ? "principiante" : levelNumber===2 ? "intermedio":"avanzado");
    }

    render(){
        const {name, level} = this.props.info;
        const stringLevel = this.getStringLevel(level);

        return(
            <React.Fragment>
                <h3>{name}</h3>
        <       p>{stringLevel}</p>
            </React.Fragment>
        );
    }
}