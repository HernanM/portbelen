import React ,{Component} from "react";
import Projects from "./Projects";

export default class ProjectsContainer extends Component{



    render(){
        const {name, level} = this.props.info;
        const stringLevel = this.getStringLevel(level);

        return(
            <React.Fragment>
                <h3>{name}</h3>
                <p>{stringLevel}</p>
            </React.Fragment>
        );
    }
}