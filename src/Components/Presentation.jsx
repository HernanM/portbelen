import React, { Component } from 'react';

export default class Presentation extends Component {

    render() {
        const {img, name, description, roll } = this.props;

        return (
            <div className="d-flex flex-column align-items-center">
                <img className="photo" height="200px" width="200px" src={img} alt=""/>
                <div className="info-container">
                    <h4>Hola me llamo</h4>
                    <h1>{name} </h1>
                    <h3>y {roll}</h3>
                </div>
                <p> {description} </p>
            </div>
        );
    }
}